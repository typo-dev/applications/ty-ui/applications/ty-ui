FROM bitnami/apache:2.4.63

USER root

RUN apt-get update && apt-get install -y --no-install-recommends curl && rm -rf /var/lib/apt/lists/*

USER 1001

ENV APACHE_HTTP_PORT_NUMBER=3000

COPY --chown=1001:1001 storybook-static/ /app

EXPOSE 3000

HEALTHCHECK --interval=30s --timeout=5s --start-period=30s --retries=3 CMD curl -f -s localhost:3000 || exit 1
