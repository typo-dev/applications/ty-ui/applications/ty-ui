help: ## Afficher ce message d'aide
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
.PHONY: help

lint-yaml: ## Lance le linter yaml
	@docker run --pull always --rm $$(tty -s && echo "-it" || echo) -v $(PWD):/data cytopia/yamllint:latest .
.PHONY: lint-yaml

lint-markdown: ## Lance le linter markdown
	@docker run --pull always --rm -v $(PWD):/app -w /app registry.gitlab.com/typo-indus/docker/markdownlint:main /bin/sh -c 'markdownlint "README.md"'
.PHONY: lint-markdown

lint-dockerfile: ## Lance le linter dockerfile
	@docker run --pull always --rm -v $(PWD):/app -w /app hadolint/hadolint:latest-debian hadolint Dockerfile
.PHONY: lint-dockerfile

linter: lint-yaml lint-markdown lint-dockerfile ## Lance tous les linters
.PHONY: linter

exec:
	@$(CONTENEUR_PHP) bash
.PHONY: exec
