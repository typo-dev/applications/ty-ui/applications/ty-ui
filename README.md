# 🌟 TY-UI

[![pipeline status](https://gitlab.com/typo-dev/applications/ty-ui/applications/ty-ui/badges/main/pipeline.svg)](https://gitlab.com/typo-dev/applications/ty-ui/applications/ty-ui/-/commits/main)
![GitLab Issues](https://img.shields.io/gitlab/issues/open/56811921)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![npm version](https://img.shields.io/npm/v/@typo13/ty-ui)](https://www.npmjs.com/package/@typo13/ty-ui)
[![npm downloads](https://img.shields.io/npm/dt/@typo13/ty-ui)](https://www.npmjs.com/package/@typo13/ty-ui)
![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/56811921)

## 📜 Introduction

Ce projet "Ty-ui" a pour but de créerune librairie répondant aux critères
d'accessibilité.
Dans un second temps, j'aimerais que le design soit personnalisable.

## 🛠️ Build With

[![React](https://img.shields.io/badge/React-61DAFB?style=for-the-badge&logo=react&logoColor=white)](https://reactjs.org/)
[![Storybook](https://img.shields.io/badge/Storybook-FF4785?style=for-the-badge&logo=storybook&logoColor=white)](https://storybook.js.org/)

## 📦 Installation

Pour installer Ty-ui, vous pouvez utiliser npm ou yarn :

```bash
npm install @typo13/ty-ui
```

## 🚀 Utilisation

Pour une documentation complète et des exemples interactifs, consultez notre
[Storybook](https://www.ty-ui.tonyschmitt.fr/).

## 🐛 Issues

Pour signaler des bugs ou suggérer des améliorations, veuillez utiliser les
issues GitLab.

## 🤝 Contribution

Les contributions sont les bienvenues !

Avant de contribuer, créé une issue pour discuter du point qui vous intéresse.

Une fois l'issue validé par nos soins, si vous souhaitez contribuer à ty-ui,
veuillez suivre ces étapes :

- Forker le projet.
- Créer une branche pour votre fonctionnalité
(git checkout -b feature/NouvelleFonctionnalite).
- Committer vos modifications (git commit -m 'Ajouter une nouvelle
fonctionnalité').
- Pusher la branche (git push origin feature/NouvelleFonctionnalite).
- Ouvrir une Merge Request.

### 🛠️ Installation en Local

Pour installer le projet en local et faire des modifications, suivez ces
étapes :

Clonez le dépôt :

```shell
git clone https://gitlab.com/typo-dev/applications/ty-ui/applications/ty-ui.git
```

Accédez au répertoire du projet :

```shell
cd ty-ui
```

Installez les dépendances :

```shell
npm install
```

Lancez le storybook :

```shell
npm storybook
```

## 📜 Licence

Ce projet est sous licence MIT. Voir le fichier LICENSE pour plus de
détails.

Merci d'utiliser TY-UI ! 🚀
