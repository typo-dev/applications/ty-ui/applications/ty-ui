import React from "react";
import {Meta, StoryObj} from "@storybook/react";
import idCardSvg from './id-card.svg';
import {Icon} from "./../../index";

const meta: Meta<typeof Icon> = {
    title: 'Components/Icon',
    component: Icon,
    tags: ['autodocs'],
}

export default meta;

type Story = StoryObj<typeof Icon>;

export const Defaut: Story = {
    name: 'Etat par défaut',
    args: {
        icon: idCardSvg,
    },
    render: (args) => <Icon {...args} />
}