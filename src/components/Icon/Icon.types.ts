export interface IconTypes {
    /**
     * URL de l'icône
     */
    icon: string;
    /**
     * Taille de l'icône
     */
    size?: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
}