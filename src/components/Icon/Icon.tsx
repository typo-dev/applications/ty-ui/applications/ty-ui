import React, {ComponentProps, CSSProperties, forwardRef} from 'react';
import {IconTypes} from "./Icon.types";
import './Icon.css';

const Icon = forwardRef<HTMLSpanElement, IconTypes & ComponentProps<"span">>(
    (
        {
            icon,
            size = 'md',
            className,
            style = {},
            ...otherProps
        }, ref
    ) => {
        const iconClass = ['tyui-icon'];
        const iconStyle: CSSProperties = {
            ...style,
            "--icon-url": "url(" + icon + ")",
        } as CSSProperties;

        if (size !== 'md') {
            iconClass.push('tyui-icon--' + size);
        }
        if (className) {
            iconClass.push(className)
        }


        return <span {...otherProps} ref={ref} className={iconClass.join(" ")} style={iconStyle} aria-hidden></span>;
    }
);

export default Icon;