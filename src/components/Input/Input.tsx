import React, {ComponentProps, forwardRef} from 'react';
import './Input.css'
import {InputTypes} from "./Input.types";
import useCSSShortcut from "../hook/useCSSShortcut";
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";

const Input = forwardRef<HTMLInputElement, InputTypes & ComponentProps<"input"> & UseCSSShortcutTypes>(
    (
        {
            maxWidth,
            className,
            style,
            ...otherProps
        },
        ref
    ) => {
        const inputClasses = ['tyui-input'];

        if (className) {
            inputClasses.push(className);
        }

        const cssStyle = useCSSShortcut({...otherProps});

        return <input {...otherProps} ref={ref} className={inputClasses.join(" ")} style={{ ...cssStyle, ...style, maxWidth: maxWidth }} />;
    }
);

export default Input;