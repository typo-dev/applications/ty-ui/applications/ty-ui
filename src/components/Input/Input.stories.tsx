import React from 'react';
import {Input} from "./../index";
import {fn} from "@storybook/test";
import {Meta, StoryObj} from "@storybook/react";
import { InputGroup, Label, LabelHint, FormMessage } from "../index";

const meta: Meta<typeof Input> = {
    title: 'Components/Input',
    component: Input,
    args: {
        onClick: fn(),
        onChange: fn(),
    },
    tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof Input>;

export const Defaut: Story = {
    name: 'Etat par défaut',
    args: {
        name: "defaut",
        id: "defaut",
        type: "text",
        required: true
    },
    render: (args) => <InputGroup>
        <Label htmlFor={args.id}>
            Champs de saisie
        </Label>
        <Input {...args} />
    </InputGroup>
}
export const Desactive: Story = {
    name: 'Etat désactivé',
    args: {
        name: "disabled",
        id: "disabled",
        type: "text",
        disabled: true,
        value: "desactive",
    },
    render: (args) => <InputGroup>
        <Label htmlFor={args.id}>
            Champs de saisie
        </Label>
        <Input {...args} />
    </InputGroup>
}
export const DesactiveAvecFoxus: Story = {
    name: 'Etat désactivé focusable',
    args: {
        name: "disabled",
        id: "disabled",
        type: "text",
        "aria-disabled": true,
        value: "desactive",
    },
    render: (args) => <InputGroup>
        <Label htmlFor={args.id}>
            Champs de saisie
        </Label>
        <Input {...args} />
    </InputGroup>
}

export const TexteAide: Story = {
    name: 'Avec texte d\'aide',
    args: {
        name: "texteAide",
        id: "texteAide",
        type: "text"
    },
    render: (args) => <InputGroup>
        <Label htmlFor={args.id}>
            Champs de saisie (optionnel)
            <LabelHint>Texte de description</LabelHint>
        </Label>
        <Input {...args} />
    </InputGroup>
}

export const Succes: Story = {
    name: 'Message de succès',
    args: {
        name: "succes",
        id: "succes",
        type: "text",
        "aria-describedby": "success-desc",
    },
    render: (args) => <InputGroup statut="success">
        <Label htmlFor={args.id}>
            Champs de saisie (optionnel)
            <LabelHint>Texte de description</LabelHint>
        </Label>
        <Input {...args} />
        <FormMessage id={args['aria-describedby'] ?? ""}>Message de validation optionnel</FormMessage>
    </InputGroup>
}

export const Avertissement: Story = {
    name: 'Message d\'avertissement',
    args: {
        name: "avertissement",
        id: "avertissement",
        type: "text",
        "aria-describedby": "warning-desc",
    },
    render: (args) => <InputGroup statut="warning">
        <Label htmlFor={args.id}>
            Champs de saisie (optionnel)
            <LabelHint>Texte de description</LabelHint>
        </Label>
        <Input {...args} />
        <FormMessage id={args['aria-describedby'] ?? ""}>Message de warning obligatoire</FormMessage>
    </InputGroup>
}

export const Erreur: Story = {
    name: 'Message d\'error',
    args: {
        name: "erreur",
        id: "erreur",
        type: "text",
        "aria-describedby": "error-desc",
    },
    render: (args) => <InputGroup statut="error">
        <Label htmlFor={args.id}>
            Champs de saisie (optionnel)
            <LabelHint>Texte de description</LabelHint>
        </Label>
        <Input {...args} />
        <FormMessage id={args['aria-describedby'] ?? ""}>Message d&apos;erreur obligatoire</FormMessage>
    </InputGroup>
}

export const LongueurMaximumDuChamps: Story = {
    name: 'Longueur maximum du champ',
    args: {
        name: "erreur",
        id: "erreur",
        type: "text",
        maxLength: 50,
        maxWidth: '10rem',
        "aria-describedby": "longueurmax-desc",
    },
    render: (args) => <InputGroup statut="error">
        <Label htmlFor={args.id}>
            Prénom
            <LabelHint>Saisissez seulement votre premier prénom</LabelHint>
        </Label>
        <Input {...args} />
        <FormMessage id={args['aria-describedby'] ?? ""}>Message d&apos;erreur obligatoire très long et même plus long que le champ.</FormMessage>
    </InputGroup>
}