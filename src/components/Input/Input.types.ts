export interface InputTypes {
    /**
     * largeur maximum de l'input
     */
    maxWidth?: string;
}