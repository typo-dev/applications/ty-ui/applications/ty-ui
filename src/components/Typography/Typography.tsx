import React, {ComponentProps, forwardRef} from 'react';
import {TypographyTypes} from "./Typography.types";
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";
import useCSSShortcut from "../hook/useCSSShortcut";
import './Typography.css';

const Typography = forwardRef<HTMLHeadingElement & HTMLParagraphElement, TypographyTypes & ComponentProps<'h1'|'h2'|'h3'|'h4'|'h5'|'h6'|'p'> & UseCSSShortcutTypes>((
    {
        as = 'p',
        align = 'left',
        children,
        className,
        style,
        ...otherProps
    }, ref
) => {
    const Component = as;

    const typographyClasses = ['tyui-typography'];

    typographyClasses.push('tyui-typography--align-' + align);

    if (className) {
        typographyClasses.push(className)
    }

    const cssStyle = useCSSShortcut({...otherProps});


    return (
        <Component
            {...otherProps}
            className={typographyClasses.join(" ")}
            style={{...cssStyle, ...style}}
            ref={ref}
        >{children}</Component>
    )
});

export default Typography;