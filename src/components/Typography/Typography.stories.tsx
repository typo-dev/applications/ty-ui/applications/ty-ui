import {Meta, StoryObj} from "@storybook/react";
import {Typography} from "./../../index";

const meta: Meta<typeof Typography> = {
    title: 'Components/Typography',
    component: Typography,
    tags: ['autodocs'],
};

export default meta;

type Story = StoryObj<typeof Typography>;

export const Defaut: Story = {
    name: 'Etat par défaut',
    args: {
        children: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut ' +
            'labore et dolore magna aliqua.'
    }
}

export const H1: Story = {
    name: 'H1',
    args: {
        as: 'h1',
        children: 'Titre H1'
    }
}

export const H2: Story = {
    name: 'H2',
    args: {
        as: 'h2',
        children: 'Titre H2'
    }
}

export const H3: Story = {
    name: 'H3',
    args: {
        as: 'h3',
        children: 'Titre H3'
    }
}

export const H4: Story = {
    name: 'H4',
    args: {
        as: 'h4',
        children: 'Titre H4'
    }
}

export const H5: Story = {
    name: 'H5',
    args: {
        as: 'h5',
        children: 'Titre H5'
    }
}

export const H6: Story = {
    name: 'H6',
    args: {
        as: 'h6',
        children: 'Titre H6'
    }
}

export const AlignLeft: Story = {
    name: 'Texte aligné à gauche',
    args: {
        align: 'left',
        children: 'Lorem ipsum dolor sit amet.'
    }
}

export const AlignRight: Story = {
    name: 'Texte aligné à droite',
    args: {
        align: 'right',
        children: 'Lorem ipsum dolor sit amet.'
    }
}

export const AlignCenter: Story = {
    name: 'Texte aligné au centre',
    args: {
        align: 'center',
        children: 'Lorem ipsum dolor sit amet.'
    }
}

export const AlignJustify: Story = {
    name: 'Texte justifié',
    args: {
        align: 'justify',
        children: 'Lorem ipsum dolor sit amet.'
    }
}