import {ReactNode} from "react";

export interface TypographyTypes {
    /**
     * Balise HTML à utiliser
     */
    as?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p';
    /**
     * Permet de choisir l'alignement du texte
     */
    align?: 'left' | 'center' | 'right' | 'justify';
    /**
     * Texte
     */
    children: ReactNode;
}