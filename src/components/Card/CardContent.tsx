import React, {ComponentProps, forwardRef} from 'react';

const CardContent = forwardRef<HTMLDivElement, ComponentProps<"div">>(
    (
        {
            children,
            className,
            ...otherProps
        }, ref
    ) => {
        let classCardContent = "tyui-card__content";
        if (className) {
            classCardContent += ` ${className}`
        }
        return <div {...otherProps} ref={ref} className={classCardContent}>{children}</div>
    }
)

export default CardContent;