import React, {ComponentProps, forwardRef} from 'react';

const Card = forwardRef<HTMLDivElement, ComponentProps<"div">>(
    (
        {
            children,
            className,
            ...otherProps
        }, ref
    ) => {
        let classCard = "tyui-card";
        if (className) {
            classCard += ` ${className}`
        }
        return <div {...otherProps} ref={ref} className={classCard}>{children}</div>
    }
)

export default Card;