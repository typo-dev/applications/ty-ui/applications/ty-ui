import React, {ComponentProps, forwardRef} from 'react';
import { Typography } from '../../index';

const CardDescription = forwardRef<HTMLDivElement, ComponentProps<"div">>(
    (
        {
            children,
            className,
            ...otherProps
        }, ref
    ) => {
        let classCardDescription = "tyui-card__description";
        if (className) {
            classCardDescription += ` ${className}`
        }
        return <Typography
            {...otherProps}
            ref={ref}
            className={classCardDescription}
        >
            {children}
        </Typography>
    }
)

export default CardDescription;