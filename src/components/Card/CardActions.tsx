import React, {ComponentProps, forwardRef} from 'react';

const CardActions = forwardRef<HTMLDivElement, ComponentProps<"div">>(
    (
        {
            children,
            className,
            ...otherProps
        }, ref
    ) => {
        let classCardActions = "tyui-card__actions";
        if (className) {
            classCardActions += ` ${className}`
        }
        return <div {...otherProps} ref={ref} className={classCardActions}>{children}</div>
    }
)

export default CardActions;