import React, {ComponentProps, forwardRef} from 'react';

const CardImage = forwardRef<HTMLDivElement, ComponentProps<"div">>(
    (
        {
            children,
            className,
            ...otherProps
        }, ref
    ) => {
        let classCardImage = "tyui-card__image";
        if (className) {
            classCardImage += ` ${className}`
        }
        return <div {...otherProps} ref={ref} className={classCardImage}>{children}</div>
    }
)

export default CardImage;