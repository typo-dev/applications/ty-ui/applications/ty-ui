import React, {ComponentProps, forwardRef} from 'react';
import { Typography } from '../../index';
import { CardTitleProps } from './Card.types';

const CardTitle = forwardRef<HTMLDivElement, CardTitleProps & ComponentProps<"div">>(
    (
        {
            as = "h3",
            children,
            className,
            ...otherProps
        }, ref
    ) => {
        let classCardTitle = "tyui-card__title";
        if (className) {
            classCardTitle += ` ${className}`
        }
        return <Typography
            as={as}
            {...otherProps}
            ref={ref}
            className={classCardTitle}
        >
            {children}
        </Typography>
    }
)

export default CardTitle;