import React, {ComponentProps, forwardRef} from 'react';

const CardMedia = forwardRef<HTMLDivElement, ComponentProps<"div">>(
    (
        {
            children,
            className,
            ...otherProps
        }, ref
    ) => {
        let classCardMedia = "tyui-card__media";
        if (className) {
            classCardMedia += ` ${className}`
        }
        return <div {...otherProps} ref={ref} className={classCardMedia}>{children}</div>
    }
)

export default CardMedia;