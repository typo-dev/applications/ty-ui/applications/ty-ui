import React, {ComponentProps, forwardRef} from 'react';

const CardDetail = forwardRef<HTMLElement, ComponentProps<"small">>(
    (
        {
            children,
            className,
            ...otherProps
        }, ref
    ) => {
        let classCardDetail = "tyui-card__detail";
        if (className) {
            classCardDetail += ` ${className}`
        }
        return <small
            {...otherProps}
            ref={ref}
            className={classCardDetail}
        >
            {children}
        </small>
    }
)

export default CardDetail;