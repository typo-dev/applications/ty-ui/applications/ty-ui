import React from "react";
import {FormMessage, InputGroup, Label, LabelHint, Select} from "./../../index";
import {fn} from "@storybook/test";
import {Meta, StoryObj} from "@storybook/react";

const meta: Meta<typeof Select> = {
    title: 'Components/Select',
    component: Select,
    args: {
        onClick: fn(),
        onChange: fn(),
    },
    tags: ['autodocs'],
};

export default meta;

type Story = StoryObj<typeof Select>;

export const Defaut: Story = {
    name: 'Etat par défaut',
    args: {
        name: "defaut",
        id: "defaut",
        required: true,
        options: [
            {name: 'Option 1', value: 'option-1'},
            {name: 'Option 2', value: 'option-2'},
            {name: 'Option 3', value: 'option-3'},
            {label: 'Groupe A', options: [
                {name: 'Option A1', value: 'option-a1'},
                {name: 'Option A2', value: 'option-a2'},
                {name: 'Option A3', value: 'option-a3'},
            ]}
        ]
    },
    render: (args) => <InputGroup>
        <Label htmlFor={args.id}>Liste déroulante</Label>
        <Select {...args} />
    </InputGroup>
}

export const Desactive: Story = {
    name: 'Etat désactivé',
    args: {
        name: "disabled",
        id: "disabled",
        disabled: true,
        options: [
            {name: 'Option 1', value: 'option-1'},
            {name: 'Option 2', value: 'option-2'},
            {name: 'Option 3', value: 'option-3'},
            {label: 'Groupe A', options: [
                    {name: 'Option A1', value: 'option-a1'},
                    {name: 'Option A2', value: 'option-a2'},
                    {name: 'Option A3', value: 'option-a3'},
                ]}
        ]
    },
    render: (args) => <InputGroup>
        <Label htmlFor={args.id}>Liste déroulante</Label>
        <Select {...args} />
    </InputGroup>
}

export const DesactiveAvecFocus: Story = {
    name: 'Etat désactivé focusable',
    args: {
        name: "disabled",
        id: "disabled",
        "aria-disabled": true,
        options: [
            {name: 'Option 1', value: 'option-1'},
            {name: 'Option 2', value: 'option-2'},
            {name: 'Option 3', value: 'option-3'},
            {label: 'Groupe A', options: [
                    {name: 'Option A1', value: 'option-a1'},
                    {name: 'Option A2', value: 'option-a2'},
                    {name: 'Option A3', value: 'option-a3'},
                ]}
        ]
    },
    render: (args) => <InputGroup>
        <Label htmlFor={args.id}>Liste déroulante</Label>
        <Select {...args} />
    </InputGroup>
}

export const TexteAide: Story = {
    name: 'Avec texte d\'aide',
    args: {
        name: "texteAide",
        id: "texteAide",
        options: [
            {name: 'Option 1', value: 'option-1'},
            {name: 'Option 2', value: 'option-2'},
            {name: 'Option 3', value: 'option-3'},
            {label: 'Groupe A', options: [
                    {name: 'Option A1', value: 'option-a1'},
                    {name: 'Option A2', value: 'option-a2'},
                    {name: 'Option A3', value: 'option-a3'},
                ]}
        ]
    },
    render: (args) => <InputGroup>
        <Label htmlFor={args.id}>
            Liste déroulante (optionnel)
            <LabelHint>Texte de description</LabelHint>
        </Label>
        <Select {...args} />
    </InputGroup>
}

export const Succes: Story = {
    name: 'Message de succès',
    args: {
        name: "succes",
        id: "succes",
        "aria-describedby": "success-desc",
        options: [
            {name: 'Option 1', value: 'option-1'},
            {name: 'Option 2', value: 'option-2'},
            {name: 'Option 3', value: 'option-3'},
            {label: 'Groupe A', options: [
                    {name: 'Option A1', value: 'option-a1'},
                    {name: 'Option A2', value: 'option-a2'},
                    {name: 'Option A3', value: 'option-a3'},
                ]}
        ]
    },
    render: (args) => <InputGroup statut="success">
        <Label htmlFor={args.id}>
            Liste déroulante (optionnel)
            <LabelHint>Texte de description</LabelHint>
        </Label>
        <Select {...args} />
        <FormMessage id={args['aria-describedby'] ?? ""}>Message de validation optionnel</FormMessage>
    </InputGroup>
}

export const Avertissement: Story = {
    name: 'Message d\'avertissement',
    args: {
        name: "avertissement",
        id: "avertissement",
        "aria-describedby": "avertissement-desc",
        options: [
            {name: 'Option 1', value: 'option-1'},
            {name: 'Option 2', value: 'option-2'},
            {name: 'Option 3', value: 'option-3'},
            {label: 'Groupe A', options: [
                    {name: 'Option A1', value: 'option-a1'},
                    {name: 'Option A2', value: 'option-a2'},
                    {name: 'Option A3', value: 'option-a3'},
                ]}
        ]
    },
    render: (args) => <InputGroup statut="warning">
        <Label htmlFor={args.id}>
            Liste déroulante (optionnel)
            <LabelHint>Texte de description</LabelHint>
        </Label>
        <Select {...args} />
        <FormMessage id={args['aria-describedby'] ?? ""}>Message de validation optionnel</FormMessage>
    </InputGroup>
}

export const Erreur: Story = {
    name: 'Message d\'error',
    args: {
        name: "erreur",
        id: "erreur",
        "aria-describedby": "erreur-desc",
        options: [
            {name: 'Option 1', value: 'option-1'},
            {name: 'Option 2', value: 'option-2'},
            {name: 'Option 3', value: 'option-3'},
            {label: 'Groupe A', options: [
                    {name: 'Option A1', value: 'option-a1'},
                    {name: 'Option A2', value: 'option-a2'},
                    {name: 'Option A3', value: 'option-a3'},
                ]}
        ]
    },
    render: (args) => <InputGroup statut="error">
        <Label htmlFor={args.id}>
            Liste déroulante (optionnel)
            <LabelHint>Texte de description</LabelHint>
        </Label>
        <Select {...args} />
        <FormMessage id={args['aria-describedby'] ?? ""}>Message de validation optionnel</FormMessage>
    </InputGroup>
}