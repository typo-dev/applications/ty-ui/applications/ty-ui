import React, {ComponentProps, forwardRef, useMemo} from 'react';
import {OptionGroupTypes, OptionTypes, SelectTypes} from "./Select.types";
import './Select.css';
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";
import useCSSShortcut from "../hook/useCSSShortcut";

const Select = forwardRef<HTMLSelectElement, SelectTypes & ComponentProps<'select'> & UseCSSShortcutTypes>(
    (
        {
            maxWidth,
            className,
            style,
            options,
            ...otherProps
        },
        ref
    ) => {
        const selectClasses = ['tyui-select'];

        if (className) {
            selectClasses.push(className);
        }

        const cssStyle = useCSSShortcut({...otherProps});

        const renderOptGroup = (optGroup: OptionGroupTypes) => {
            const {options: optionsOfGroup, ...otherOptGroupProps} = optGroup;
            return <optgroup key={optGroup.label} {...otherOptGroupProps}>{optionsOfGroup.map(renderOption)}</optgroup>;
        }

        const renderOption = (option: OptionTypes) => {
            const {name, ...otherOptionProps} = option;
            return <option key={option.value} {...otherOptionProps}>{name}</option>;
        }

        const optionsRendered = useMemo(() => {
            return options.map((option) => {
                if (option.hasOwnProperty('value')) {
                    return renderOption(option as OptionTypes)
                } else {
                    return renderOptGroup(option as OptionGroupTypes)
                }
            });
        }, [options])


        return <select {...otherProps} className={selectClasses.join(" ")} style={{ ...cssStyle, ...style, maxWidth: maxWidth }} ref={ref}>
            {optionsRendered}
        </select>;
    }
)

export default Select;