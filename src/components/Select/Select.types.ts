import {ComponentProps, ReactNode} from "react";

export interface SelectTypes {
    /**
     * largeur maximum du select
     */
    maxWidth?: string;
    /**
     * Options ou OptionGroup
     */
    options: Array<OptionTypes|OptionGroupTypes>;

}

export interface OptionTypes extends ComponentProps<"option"> {
    /**
     * valeur de l'option
     */
    value: string;
    /**
     * Libelle de l'option
     */
    name: ReactNode;
}

export interface OptionGroupTypes extends ComponentProps<"optgroup"> {
    /**
     * Label du groupe d'options
     */
    label: string;
    /**
     * options
     */
    options: OptionTypes[];
}
