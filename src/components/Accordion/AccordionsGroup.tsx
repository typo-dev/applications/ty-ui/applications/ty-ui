import React, {ComponentProps, forwardRef} from 'react';
import './Accordion.css';
import AccordionProvider from "./AccordionContext";
import {AccordionsGroupType} from "./Accordion.types";

const AccordionsGroup = forwardRef<HTMLDivElement, AccordionsGroupType & ComponentProps<'div'>>(
    (
        {
            defaultIdsOpens = [],
            children,
            className,
            style,
            ...otherProps
        },
        ref
    ) => {
        let classAccordionsGroup = 'tyui-accordions-group'

        if (className) {
            classAccordionsGroup += ` ${className}`;
        }

        return <div {...otherProps} className={classAccordionsGroup} style={style} ref={ref}>
            <AccordionProvider defaultIdsOpens={defaultIdsOpens}>
                {children}
            </AccordionProvider>
        </div>
    }
)

export default AccordionsGroup;