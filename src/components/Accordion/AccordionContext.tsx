import React, {ReactNode, useContext, useState} from "react";
import {AccordionContextType} from "./Accordion.types";

export const AccordionContext = React.createContext<AccordionContextType | null>(null);

export const useAccordionContext = () => {
    const currentAccordionContext = useContext(AccordionContext);

    if (!currentAccordionContext) {
        throw new Error('useAccordionContext must be used within AccordionContext');
    }

    return currentAccordionContext;
}

const AccordionProvider: React.FC<{defaultIdsOpens?: string[], children: ReactNode}> = ({ defaultIdsOpens = [], children }) => {
    const [idsOpens, setIdsOpens] = useState<string[]>(defaultIdsOpens);

    const toggleAccordion = (id: string) => {
        if (idsOpens.includes(id)) {
            setIdsOpens(idsOpens.filter((idOpen) => id !== idOpen));
        } else {
            setIdsOpens([id]);
        }
    }

    return <AccordionContext.Provider value={{
        idsOpens,
        toggleAccordion
    }}>{children}</AccordionContext.Provider>
}

export default AccordionProvider;