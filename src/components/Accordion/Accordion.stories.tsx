import React from 'react';
import {Meta, StoryObj} from "@storybook/react";
import {Accordion, AccordionsGroup} from "./../../index";

const meta: Meta<typeof Accordion> = {
    title: 'Components/Accordion',
    component: Accordion,
    tags: ['autodocs'],
}
export default meta;

type Story = StoryObj<typeof Accordion>;

export const Defaut: Story = {
    name: 'Etat par défaut',
    args: {
        title: 'Accordéon 1',
        id: 'accordion-1',
        children: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut " +
            "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi " +
            "ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse " +
            "cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa " +
            "qui officia deserunt mollit anim id est laborum."
    },
    render: (args) => <AccordionsGroup>
        <Accordion {...args} />
        <Accordion {...args} title="Accordéon 2" id="accordion-2" />
        <Accordion {...args} title="Accordéon 3" id="accordion-3" />
    </AccordionsGroup>
}