import React, {ComponentProps, forwardRef} from 'react';
import './Accordion.css';
import {useAccordionContext} from "./AccordionContext";
import {AccordionType} from "./Accordion.types";

const Accordion = forwardRef<HTMLDivElement, AccordionType & ComponentProps<'div'>>(
    (
        {
            id,
            title,
            children,
            className,
            style,
            ...otherProps
        },
        ref
    ) => {
        let classAccordion = 'tyui-accordion-panel'

        const { idsOpens, toggleAccordion } = useAccordionContext();

        if (className) {
            classAccordion += ` ${className}`;
        }

        return <>
            <h3 className='tyui-accordion__title'>
                <button
                    type="button"
                    className={'tyui-accordion__button'}
                    aria-expanded={idsOpens.includes(id)}
                    aria-controls={id + '-panel'}
                    id={id}
                    onClick={() => toggleAccordion(id)}
                >{title}</button>
            </h3>
            <div
                {...otherProps}
                id={id + '-panel'}
                role="region"
                aria-labelledby={id}
                className={classAccordion}
                style={style}
                ref={ref}
                hidden={!idsOpens.includes(id)}
            >
                {children}
            </div>
        </>
    }
)

export default Accordion;