export interface AccordionsGroupType {
    defaultIdsOpens?: string[];
}

export interface AccordionType {
    id: string;
}

export type AccordionContextType = {
    idsOpens: string[];
    toggleAccordion: (id: string) => void;
}