import React, {ComponentProps, forwardRef} from 'react';
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";
import useCSSShortcut from "../hook/useCSSShortcut";
import {ColTypes} from "./Grid.types";

const Col = forwardRef<HTMLDivElement, ColTypes & ComponentProps<"div"> & UseCSSShortcutTypes>(
    (
        {
            col,
            colGS,
            colUS,
            colXS,
            colSM,
            colMD,
            colLG,
            colXL,
            colUL,
            colGL,
            offset,
            offsetGS,
            offsetUS,
            offsetXS,
            offsetSM,
            offsetMD,
            offsetLG,
            offsetXL,
            offsetUL,
            offsetGL,
            children,
            className,
            style,
            ...otherProps
        }, ref
    ) => {

        const classCol = ['tyui-col'];

        if (className) {
            classCol.push(className);
        }

        if (col) {
            classCol.push('tyui-col-' + col.toString())
        }

        if (colGS) {
            classCol.push('tyui-col-gs-' + colGS.toString())
        }

        if (colUS) {
            classCol.push('tyui-col-us-' + colUS.toString())
        }

        if (colXS) {
            classCol.push('tyui-col-xs-' + colXS.toString())
        }

        if (colSM) {
            classCol.push('tyui-col-sm-' + colSM.toString())
        }

        if (colMD) {
            classCol.push('tyui-col-md-' + colMD.toString())
        }

        if (colLG) {
            classCol.push('tyui-col-lg-' + colLG.toString())
        }

        if (colXL) {
            classCol.push('tyui-col-xl-' + colXL.toString())
        }

        if (colUL) {
            classCol.push('tyui-col-ul-' + colUL.toString())
        }

        if (colGL) {
            classCol.push('tyui-col-gl-' + colGL.toString())
        }


        if (offset) {
            classCol.push('tyui-col-offset-' + offset.toString())
        }

        if (offsetGS) {
            classCol.push('tyui-col-offset-gs-' + offsetGS.toString())
        }

        if (offsetUS) {
            classCol.push('tyui-col-offset-us-' + offsetUS.toString())
        }

        if (offsetXS) {
            classCol.push('tyui-col-offset-xs-' + offsetXS.toString())
        }

        if (offsetSM) {
            classCol.push('tyui-col-offset-sm-' + offsetSM.toString())
        }

        if (offsetMD) {
            classCol.push('tyui-col-offset-md-' + offsetMD.toString())
        }

        if (offsetLG) {
            classCol.push('tyui-col-offset-lg-' + offsetLG.toString())
        }

        if (offsetXL) {
            classCol.push('tyui-col-offset-xl-' + offsetXL.toString())
        }

        if (offsetUL) {
            classCol.push('tyui-col-offset-ul-' + offsetUL.toString())
        }

        if (offsetGL) {
            classCol.push('tyui-col-offset-gl-' + offsetGL.toString())
        }

        const cssStyle = useCSSShortcut({...otherProps});

        return <div className={classCol.join(" ")} style={{...cssStyle, ...style}} ref={ref}>{children}</div>
    }
)

export default Col;