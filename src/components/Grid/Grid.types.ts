export interface RowTypes {
    /**
     * Ajoute des espaces entre les colonnes (gap)
     */
    gap?: boolean;
}

type ColValues = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;

export interface ColTypes {
    /**
     * Nombre de colonnes
     */
    col?: ColValues;
    /**
     * Nombre de colonnes après le breakpoint GS
     */
    colGS?: ColValues;
    /**
     * Nombre de colonnes après le breakpoint US
     */
    colUS?: ColValues;
    /**
     * Nombre de colonnes après le breakpoint XS
     */
    colXS?: ColValues;
    /**
     * Nombre de colonnes après le breakpoint SM
     */
    colSM?: ColValues;
    /**
     * Nombre de colonnes après le breakpoint MD
     */
    colMD?: ColValues;
    /**
     * Nombre de colonnes après le breakpoint LG
     */
    colLG?: ColValues;
    /**
     * Nombre de colonnes après le breakpoint XL
     */
    colXL?: ColValues;
    /**
     * Nombre de colonnes après le breakpoint UL
     */
    colUL?: ColValues;
    /**
     * Nombre de colonnes après le breakpoint GL
     */
    colGL?: ColValues;
    /**
     * Nombre de colonnes de décalage
     */
    offset?: ColValues;
    /**
     * Nombre de colonnes de décalage après le breakpoint GS
     */
    offsetGS?: ColValues;
    /**
     * Nombre de colonnes de décalage après le breakpoint US
     */
    offsetUS?: ColValues;
    /**
     * Nombre de colonnes de décalage après le breakpoint XS
     */
    offsetXS?: ColValues;
    /**
     * Nombre de colonnes de décalage après le breakpoint SM
     */
    offsetSM?: ColValues;
    /**
     * Nombre de colonnes de décalage après le breakpoint MD
     */
    offsetMD?: ColValues;
    /**
     * Nombre de colonnes de décalage après le breakpoint LG
     */
    offsetLG?: ColValues;
    /**
     * Nombre de colonnes de décalage après le breakpoint XL
     */
    offsetXL?: ColValues;
    /**
     * Nombre de colonnes de décalage après le breakpoint UL
     */
    offsetUL?: ColValues;
    /**
     * Nombre de colonnes de décalage après le breakpoint GL
     */
    offsetGL?: ColValues;
}