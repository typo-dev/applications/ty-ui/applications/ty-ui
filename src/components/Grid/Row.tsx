import React, {ComponentProps, forwardRef} from 'react';
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";
import useCSSShortcut from "../hook/useCSSShortcut";
import {RowTypes} from "./Grid.types";

const Row = forwardRef<HTMLDivElement, RowTypes & ComponentProps<"div"> & UseCSSShortcutTypes>(
    (
        {
            gap = false,
            children,
            className,
            style,
            ...otherProps
        }, ref
    ) => {

        const classRow = ['tyui-row'];

        if (gap) {
            classRow.push('tyui-row--gap');
        }

        if (className) {
            classRow.push(className);
        }

        const cssStyle = useCSSShortcut({...otherProps});

        return <div className={classRow.join(" ")} style={{...cssStyle, ...style}} ref={ref}>{children}</div>
    }
)

export default Row;