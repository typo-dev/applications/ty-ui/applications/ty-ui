import {Meta, StoryObj} from "@storybook/react";
import {Row, Col} from "./../../index";
import React from "react";

const meta: Meta<typeof Col> = {
    title: 'Components/Grid',
    component: Col,
    tags: ['autodocs']
}

export default meta;

type Story = StoryObj<typeof Col>;

export const Defaut: Story = {
    name: 'Défaut',
    args: {
        col: 12
    },
    render: (args) => <Row><Col {...args} style={{ background: "red" }}>Lorem ipsum.</Col></Row>
}


export const Col4: Story = {
    name: 'Colonne de taille 4',
    args: {
        col: 4
    },
    render: (args) => <Row><Col {...args} style={{ background: "red" }}>Lorem ipsum.</Col></Row>
}

export const Col4Offset4: Story = {
    name: 'Colonne de taille 4 déplacé de 4',
    args: {
        col: 4,
        offset: 4
    },
    render: (args) => <Row><Col {...args} style={{ background: "red" }}>Lorem ipsum.</Col></Row>
}


export const ColMD4: Story = {
    name: 'Colonne de taille 4 après md',
    args: {
        col: 12,
        colMD: 4
    },
    render: (args) => <Row><Col {...args} style={{ background: "red" }}>Lorem ipsum.</Col></Row>
}


export const ColGuutersMD4: Story = {
    name: 'Colonne de taille 4 après md avec gouttière',
    args: {
        col: 12,
        colMD: 4
    },
    render: (args) => <Row gutters><Col {...args} style={{ background: "red" }}>Lorem ipsum.</Col></Row>
}