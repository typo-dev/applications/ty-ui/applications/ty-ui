import {Meta, StoryObj} from "@storybook/react";
import {Container} from "./../../index";
import React from "react";

const meta: Meta<typeof Container> =  {
    title: 'Components/Container',
    component: Container,
    tags: ['autodocs']
}

export default meta;

type Story = StoryObj<typeof Container>;

export const Defaut: Story = {
    name: 'Défaut',
    args: {
        children: <div style={{ background: 'yellow', width: '100%' }}>Lorem Ipsum...</div>
    }
}

export const Fluid: Story = {
    name: 'Sans taille maximum',
    args: {
        fluid: true,
        children: <div style={{ background: 'yellow', width: '100%' }}>Lorem Ipsum...</div>
    }
}
