import React, {ComponentProps, forwardRef} from 'react';
import {ContainerTypes} from "./Container.types";
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";
import useCSSShortcut from "../hook/useCSSShortcut";
import './Container.css';

const Container = forwardRef<HTMLDivElement, ContainerTypes & ComponentProps<"div"> & UseCSSShortcutTypes>(
    (
        {
            children,
            fluid = false,
            className,
            style,
            ...otherProps
        }, ref
    ) => {
        const classContainer = ['tyui-container'];

        if (fluid) {
            classContainer.push('tyui-container--fluid');
        }

        if (className) {
            classContainer.push(className);
        }

        const cssStyle = useCSSShortcut({...otherProps});

        return <div className={classContainer.join(" ")} style={{...cssStyle, ...style}} ref={ref}>{children}</div>
    }
);

export default Container;