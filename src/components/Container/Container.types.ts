import {ReactNode} from "react";

export interface ContainerTypes {
    /**
     * Contenu
     */
    children: ReactNode;
    /**
     * Enlever la taille maximum
     */
    fluid?: boolean;
}