import React, {ComponentProps, forwardRef} from 'react';
import './Pill.css';

const Pill = forwardRef<HTMLSpanElement, ComponentProps<'span'>>(
    (
        {
            children,
            className,
            ...otherProps
        },
        ref
    ) => {
        let classPill = 'tyui-pill'

        if (className) {
            classPill += ` ${className}`;
        }

        return <span className={classPill} {...otherProps} ref={ref}>
            {children}
        </span>
    }
)

export default Pill;