import React, {ComponentProps, forwardRef} from 'react';
import './Pill.css';

const PillsGroup = forwardRef<HTMLUListElement, ComponentProps<'ul'>>(
    (
        {
            children,
            className,
            ...otherProps
        },
        ref
    ) => {
        let classPillsGroup = 'tyui-pills-group'

        if (className) {
            classPillsGroup += ` ${className}`;
        }

        return <ul className={classPillsGroup} {...otherProps} ref={ref}>
            {children}
        </ul>
    }
)

export default PillsGroup;