import {Meta, StoryObj} from "@storybook/react";
import {Pill} from "./../index";

const meta: Meta<typeof Pill> = {
    title: 'Components/Pill',
    component: Pill,
    args: {},
    tags: ['autodocs']
}

export default meta;

type Story = StoryObj<typeof Pill>;

export const Defaut: Story = {
    name: 'Défaut',
    args: {
        children: 'Pill label'
    }
}