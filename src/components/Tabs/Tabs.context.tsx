import React, {ReactNode, useContext, useState} from 'react';
import {TabsContextType} from "./Tabs.types";

export const TabsContext = React.createContext<TabsContextType | null>(null);

export const useTabsContext = () => {
    const currentTabsContext = useContext(TabsContext);

    if (!currentTabsContext) {
        throw new Error('useTabsContext must be used within TabsContext');
    }

    return currentTabsContext;
}

const TabsProvider: React.FC<{defaultIdTabSelected: string, children: ReactNode}> = ({ defaultIdTabSelected, children }) => {
    const [idTabSelected, setIdTabSelected] = useState<string>(defaultIdTabSelected);

    return <TabsContext.Provider value={{
        idTabSelected,
        setIdTabSelected
    }}>{children}</TabsContext.Provider>
}

export default TabsProvider;