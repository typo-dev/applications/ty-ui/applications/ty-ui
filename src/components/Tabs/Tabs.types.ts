export interface TabsTypes {
    /**
     * Id de l'onglet sélectionné par défaut
     */
    defaultIdTabSelected: string;
    /**
     * Permet de prendre toute la largeur disponible pour les onglets
     */
    allWidth?: boolean;
}

export interface TabListTypes {
    /**
     * Nom du système d'onglet
     */
    label: string;
}

export interface TabTypes {
    /**
     * id de l'onglet unique le même que le composant TabPanel correspondant
     */
    idTab: string;
    /**
     * url de l'icône
     */
    icon?: string;
    /**
     * Position de l'icône
     */
    iconPosition?: 'left' | 'right';
}

export interface TabPanelTypes {
    /**
     id de l'onglet unique le même que le composant Tab correspondant
     */
    idTab: string;
}

export type TabsContextType = {
    idTabSelected: string;
    setIdTabSelected: (idTab: string) => void;
}