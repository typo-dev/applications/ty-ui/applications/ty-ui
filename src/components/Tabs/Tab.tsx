import React, {ComponentProps, CSSProperties, forwardRef, KeyboardEventHandler, useRef} from 'react';
import {TabTypes} from "./Tabs.types";
import {useTabsContext} from "./Tabs.context";
import {useMergeRefs} from "../../useMergeRefs";

const Tab = forwardRef<HTMLButtonElement, TabTypes & ComponentProps<"button">>(
    (
        {
            idTab,
            icon,
            iconPosition,
            children,
            className,
            style = {},
            ...otherProps
        }, ref
    ) => {
        const refTab = useRef<HTMLButtonElement>(null);
        const mergedRef = useMergeRefs([ref, refTab]);
        const {idTabSelected, setIdTabSelected} = useTabsContext();

        const tabsClass = ["tyui-tabs__tab"];
        let tabsStyle = {...style};


        if (icon) {
            tabsClass.push('tyui-tabs__tab--icon');
            tabsStyle = {
                ...tabsStyle,
                '--icon-url': "url(" + icon + ")",
            } as CSSProperties;
            if (iconPosition) {
                tabsClass.push(`tyui-tabs__tab--icon-${iconPosition}`);
            }
        }

        if (className) {
            tabsClass.push(className);
        }

        const handleKeydownEvent: KeyboardEventHandler<HTMLButtonElement> = (e) => {
            if (!refTab || !refTab.current) {
                return;
            }
            if (e.key === 'ArrowRight') {
                const nextElement = refTab.current.nextElementSibling as HTMLElement;
                if (nextElement) {
                    nextElement.focus();
                    e.preventDefault();
                    e.stopPropagation();
                } else {
                    const firstElement = refTab.current.parentElement?.firstElementChild as HTMLElement;
                    if (firstElement) {
                        firstElement.focus();
                        e.preventDefault();
                        e.stopPropagation();
                    }
                }
            }

            if (e.key === 'ArrowLeft') {
                const prevElement = refTab.current.previousElementSibling as HTMLElement;
                if (prevElement) {
                    prevElement.focus();
                    e.preventDefault();
                    e.stopPropagation();
                } else {
                    const lastElement = refTab.current.parentElement?.lastElementChild as HTMLElement;
                    if (lastElement) {
                        lastElement.focus();
                        e.preventDefault();
                        e.stopPropagation();
                    }
                }
            }
            if (e.key === 'Home') {
                const firstElement = refTab.current.parentElement?.firstElementChild as HTMLElement;
                if (firstElement) {
                    firstElement.focus();
                    e.preventDefault();
                    e.stopPropagation();
                }
            }

            if (e.key === 'End') {
                const lastElement = refTab.current.parentElement?.lastElementChild as HTMLElement;
                if (lastElement) {
                    lastElement.focus();
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        }

        return <button
            {...otherProps}
            ref={mergedRef}
            className={tabsClass.join(" ")}
            style={tabsStyle}
            id={idTab}
            type="button"
            role="tab"
            tabIndex={idTabSelected === idTab ? 0 : -1}
            aria-selected={idTabSelected === idTab}
            aria-controls={idTab + '-panel'}
            onClick={() => {
                setIdTabSelected(idTab);
            }}
            onKeyDown={handleKeydownEvent}
        ><span className="tyui-tabs__tab-name">{children}</span></button>;

    }
);

export default Tab;