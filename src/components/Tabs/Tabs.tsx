import React, {ComponentProps, forwardRef} from 'react';
import {TabsTypes} from "./Tabs.types";
import TabsProvider from "./Tabs.context";
import './Tabs.css';

const Tabs = forwardRef<HTMLDivElement, TabsTypes & ComponentProps<"div">>(
    (
        {
            defaultIdTabSelected,
            allWidth,
            children,
            className,
            style,
            ...otherProps
        }, ref
    ) => {
        const tabsClass = ["tyui-tabs"];

        if (allWidth) {
            tabsClass.push('tyui-tabs__all-width');
        }

        if (className) {
            tabsClass.push(className);
        }

        return <div {...otherProps} ref={ref} className={tabsClass.join(" ")} style={style}>
            <TabsProvider defaultIdTabSelected={defaultIdTabSelected}>
                {children}
            </TabsProvider>
        </div>;
    }
);

export default Tabs;