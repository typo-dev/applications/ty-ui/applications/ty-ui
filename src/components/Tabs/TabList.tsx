import React, {ComponentProps, forwardRef} from 'react';
import {TabListTypes} from "./Tabs.types";

const TabList = forwardRef<HTMLDivElement, TabListTypes & ComponentProps<"div">>(
    (
        {
            label,
            children,
            className,
            style,
            ...otherProps
        }, ref
    ) => {
        const tabsClass = ["tyui-tabs__tablist"];

        if (className) {
            tabsClass.push(className);
        }

        return <div
            {...otherProps}
            ref={ref}
            className={tabsClass.join(" ")}
            style={style}
            aria-label={label}
            role='tablist'
        >{children}</div>;
    }
);

export default TabList;