import React from 'react';
import {Meta, StoryObj} from "@storybook/react";
import {Tab, TabList, TabPanel, Tabs} from "./../../index";
import swordClashIcon from './sword-clash.svg';

const meta: Meta<typeof Tabs> = {
    title: 'Components/Tabs',
    component: Tabs,
    tags: ['autodocs'],
}

export default meta;

type Story = StoryObj<typeof Tabs>;

export const Defaut: Story = {
    name: 'Etat par défaut',
    args: {},
    render: () => (
        <>
            <Tabs defaultIdTabSelected="tabA-1">
                <TabList label="Nom du système d'onglet">
                    <Tab idTab="tabA-1">Tab 1</Tab>
                    <Tab idTab="tabA-2">Tab 2</Tab>
                    <Tab idTab="tabA-3">Tab 3</Tab>
                </TabList>
                <TabPanel idTab="tabA-1">Content tab 1</TabPanel>
                <TabPanel idTab="tabA-2">Content tab 2</TabPanel>
                <TabPanel idTab="tabA-3">Content tab 3</TabPanel>
            </Tabs>
        </>
    )
}


export const WithIcon: Story = {
    name: 'Avec icône seule',
    args: {},
    render: () => (
        <>
            <Tabs defaultIdTabSelected="tabB-1">
                <TabList label="Nom du système d'onglet">
                    <Tab idTab="tabB-1" icon={swordClashIcon}>Tab 1</Tab>
                    <Tab idTab="tabB-2" icon={swordClashIcon}>Tab 2</Tab>
                    <Tab idTab="tabB-3" icon={swordClashIcon}>Tab 3</Tab>
                </TabList>
                <TabPanel idTab="tabB-1">Content tab 1</TabPanel>
                <TabPanel idTab="tabB-2">Content tab 2</TabPanel>
                <TabPanel idTab="tabB-3">Content tab 3</TabPanel>
            </Tabs>
        </>
    )
}

export const WithIconLeft: Story = {
    name: 'Avec icône à gauche',
    args: {},
    render: () => (
        <>
            <Tabs defaultIdTabSelected="tabC-1">
                <TabList label="Nom du système d'onglet">
                    <Tab idTab="tabC-1" icon={swordClashIcon} iconPosition="left">Tab 1</Tab>
                    <Tab idTab="tabC-2" icon={swordClashIcon} iconPosition="left">Tab 2</Tab>
                    <Tab idTab="tabC-3" icon={swordClashIcon} iconPosition="left">Tab 3</Tab>
                </TabList>
                <TabPanel idTab="tabC-1">Content tab 1</TabPanel>
                <TabPanel idTab="tabC-2">Content tab 2</TabPanel>
                <TabPanel idTab="tabC-3">Content tab 3</TabPanel>
            </Tabs>
        </>
    )
}

export const WithIconRight: Story = {
    name: 'Avec icône à droite',
    args: {},
    render: () => (
        <>
            <Tabs defaultIdTabSelected="tabD-1">
                <TabList label="Nom du système d'onglet">
                    <Tab idTab="tabD-1" icon={swordClashIcon} iconPosition="right">Tab 1</Tab>
                    <Tab idTab="tabD-2" icon={swordClashIcon} iconPosition="right">Tab 2</Tab>
                    <Tab idTab="tabD-3" icon={swordClashIcon} iconPosition="right">Tab 3</Tab>
                </TabList>
                <TabPanel idTab="tabD-1">Content tab 1</TabPanel>
                <TabPanel idTab="tabD-2">Content tab 2</TabPanel>
                <TabPanel idTab="tabD-3">Content tab 3</TabPanel>
            </Tabs>
        </>
    )
}


export const AllWidth: Story = {
    name: 'Etat prend toute la largeur',
    args: {},
    render: () => (
        <>
            <Tabs defaultIdTabSelected="tabE-1" allWidth>
                <TabList label="Nom du système d'onglet">
                    <Tab idTab="tabE-1">Tab 1</Tab>
                    <Tab idTab="tabE-2">Tab 2</Tab>
                    <Tab idTab="tabE-3">Tab 3</Tab>
                </TabList>
                <TabPanel idTab="tabE-1">Content tab 1</TabPanel>
                <TabPanel idTab="tabE-2">Content tab 2</TabPanel>
                <TabPanel idTab="tabE-3">Content tab 3</TabPanel>
            </Tabs>
        </>
    )
}


export const AllWidthIcon: Story = {
    name: 'Etat prend toute la largeur avec icône',
    args: {},
    render: () => (
        <>
            <Tabs defaultIdTabSelected="tabF-1" allWidth>
                <TabList label="Nom du système d'onglet">
                    <Tab idTab="tabF-1" icon={swordClashIcon} iconPosition="left">Tab 1</Tab>
                    <Tab idTab="tabF-2" icon={swordClashIcon} iconPosition="left">Tab 2</Tab>
                    <Tab idTab="tabF-3" icon={swordClashIcon} iconPosition="left">Tab 3</Tab>
                </TabList>
                <TabPanel idTab="tabF-1">Content tab 1</TabPanel>
                <TabPanel idTab="tabF-2">Content tab 2</TabPanel>
                <TabPanel idTab="tabF-3">Content tab 3</TabPanel>
            </Tabs>
        </>
    )
}