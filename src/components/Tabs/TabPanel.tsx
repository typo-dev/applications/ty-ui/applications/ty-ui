import React, {ComponentProps, forwardRef} from 'react';
import {TabPanelTypes} from "./Tabs.types";
import {useTabsContext} from "./Tabs.context";

const TabPanel = forwardRef<HTMLDivElement, TabPanelTypes & ComponentProps<"div">>(
    (
        {
            idTab,
            children,
            className,
            style,
            ...otherProps
        }, ref
    ) => {
        const {idTabSelected} = useTabsContext();

        const tabsClass = ["tyui-tabs__tabpanel"];

        if (className) {
            tabsClass.push(className);
        }

        return <div
            {...otherProps}
            ref={ref}
            className={tabsClass.join(" ")}
            style={style}
            id={idTab + '-panel'}
            aria-labelledby={idTab}
            role="tabpanel"
            tabIndex={0}
            hidden={idTabSelected !== idTab}
        >{children}</div>;
    }
);

export default TabPanel;