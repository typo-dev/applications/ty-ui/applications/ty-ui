import React, {ComponentProps, forwardRef} from 'react';
import './Button.css';

const ButtonsGroup = forwardRef<HTMLUListElement, ComponentProps<'ul'>>(
    (
        {
            children,
            className,
            ...otherProps
        },
        ref
    ) => {
        let classButtonsGroup = 'tyui-buttons-group'

        if (className) {
            classButtonsGroup += ` ${className}`;
        }

        return <ul className={classButtonsGroup} {...otherProps} ref={ref}>
            {children}
        </ul>
    }
)

export default ButtonsGroup;