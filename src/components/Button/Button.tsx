import React, {ComponentProps, CSSProperties, forwardRef} from 'react';
import './Button.css';
import {ButtonProps} from "./Button.types";
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";
import useCSSShortcut from "../hook/useCSSShortcut";

const Button = forwardRef<HTMLButtonElement, ButtonProps & ComponentProps<"button"> & UseCSSShortcutTypes>(
    (
        {
            icon,
            iconPosition,
            children,
            onClick,
            variant,
            className,
            style,
            ...otherProps
        },
        ref
    ) => {

        const cssStyle = useCSSShortcut({...otherProps});

        let styleButton = {...cssStyle, ...style};

        const buttonClasses = [
            "tyui-button",
        ];

        if (className) {
            buttonClasses.push(className);
        }

        if (variant) {
            buttonClasses.push(`tyui-button--${variant}`)
        }

        if (icon) {
            buttonClasses.push('tyui-button--icon');
            styleButton = {
                ...styleButton,
                '--icon-url': "url(" + icon + ")",
            } as CSSProperties;
            if (iconPosition) {
                buttonClasses.push(`tyui-button--icon-${iconPosition}`);
            }
        }


        return <button
            {...otherProps}
            style={styleButton}
            className={buttonClasses.join(" ")}
            onClick={onClick}
            ref={ref}
        >{children}</button>;
    }
);

export default Button;