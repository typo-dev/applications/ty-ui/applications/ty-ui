import {ArgTypes, Canvas, Meta} from '@storybook/blocks';

import * as ButtonStories from './Button.stories';
import Button from "./Button";


<Meta of={ButtonStories} />

# Button

Use buttons to help users perform an action on a page, such as initiating a search or saving their progress.

## Anatomy of a Button

A button is composed of:
- Text, with color, height, and thickness
- Background color
- Top and bottom internal margins
- Left and right internal margins
- External margins
- Border

### Styles

All buttons share common characteristics:
- The text size is 1rem with a line-height of 1.25rem and a font-weight of 700.
- The button is at least 44px wide and tall.
- The left and right internal margins are 1rem and are twice as large as the top and bottom margins
(this is generally recommended).
- The button has only one size (at least for now).

## Variations

### Primary Button

Use the primary button for the main action on the page. The primary button should appear only once per screen
(excluding the header, modals, and side panel).

#### Styles

This is the only button that has a background color. The background have the primary color.

<Canvas of={ButtonStories.PrimaryButton} />

#### Désactivé

<Canvas of={ButtonStories.PrimaryDisabledButton} />


### Secondary Button

The secondary button should be used by default for all other buttons on the screen. It is less prioritized than
the primary button and distinct from links.

#### Styles

Some design systems propose a "gray/black" secondary button. We did not make this choice for the following reasons:
- For colorblind users, the difference with the primary button might not be noticeable.
- The button could be confused with the "disabled" state.

Therefore, the button has outlines and text in the primary color.

<Canvas of={ButtonStories.SecondaryButton} />

#### Désactivé

<Canvas of={ButtonStories.SecondaryDisabledButton} />


### Bouton tertiaire

The tertiary button is reserved for contextual, alternative actions with low priority on a screen.
Examples include closing a modal, canceling, or going back.

#### Styles

The tertiary button is borderless, which gives it a subtle and less prominent appearance. This design choice ensures
that it does not draw attention away from more important actions on the screen. The lack of a border makes it blend
seamlessly into the background, maintaining a clean and uncluttered interface. This is particularly useful for actions
that are less critical but still necessary, such as closing a modal or canceling an operation.

<Canvas of={ButtonStories.TertiaryButton} />

#### Désactivé

<Canvas of={ButtonStories.TertiaryDisabledButton} />


### Destructive Button

Use the destructive button for actions that could have destructive effects on the user's data
(e.g., deleting or removing). For example, in a modal confirming data deletion. The destructive button is a derivative
of the primary button in red. Therefore, there can only be one primary/destructive button per screen.

#### Styles

Adopts the design of the primary button but with a red color to indicate its destructive nature.

<Canvas of={ButtonStories.DestructiveButton} />

#### Désactivé

<Canvas of={ButtonStories.DestructiveDisabledButton} />


## Rules to Apply

Here is a list of rules you should apply each time you use a button.

- Respect the variation according to the usage
- A button should not be a link. It should be used to trigger an action.
- The label must be explicit.
- If the label is not explicit (not recommended), you should use the title or aria-label attribute. In the case of aria-label, the value should match the visible label.
- If possible, the label should have the following format:
  - In all cases, start with an action verb and use a maximum of 3 words
  - If the button opens a modal, it is customary to add "..." at the end of the label
- Avoid using a disabled button as much as possible. On a form, it is preferable for the button to be clickable, which helps highlight missing/erroneous fields.
- If you must use a disabled button, prefer the use of the disabled attribute over aria-disabled.

## Check by the Library

- The button has a sufficiently large clickable area (44x44px)
- Contrasts are respected

## Customise

- Color : you can customize the colors by replacing the primary color variables.
- border-radius : change the value of ``--border-radius``


## FAQ

Nothing

## Props

### Button

<ArgTypes of={Button} />

## Changelog

### 0.5.0

- Refactor of the color system and its application to buttons. Only disabled buttons change color.
- No more difference between ``aria-disabled`` and ``disabled``.
- Addition of documentation.