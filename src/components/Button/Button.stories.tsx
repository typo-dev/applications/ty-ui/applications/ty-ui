import React from 'react';
import {Button} from "./../index";
import {Meta, StoryObj} from "@storybook/react";
import { fn } from '@storybook/test';
import swordClashIcon from './sword-clash.svg';

const meta: Meta<typeof Button> = {
    title: 'Components/Button',
    component: Button,
    args: { onClick: fn() },
    tags: [],
};

export default meta;
type Story = StoryObj<typeof meta>;

export const PrimaryButton: Story = {
    args: {
        children: 'Primary button',
        variant: 'primary',
    }
}
export const PrimaryDisabledButton: Story = {
    args: {
        children: 'Primary disabled button',
        variant: 'primary',
        disabled: true,
    }
}
export const SecondaryButton: Story = {
    args: {
        children: 'Secondary button',
        variant: 'secondary',
    }
}
export const SecondaryDisabledButton: Story = {
    args: {
        children: 'Secondary disabled button',
        variant: 'secondary',
        disabled: true,
    }
}
export const TertiaryButton: Story = {
    args: {
        children: 'Tertiary button',
        variant: 'tertiary',
    }
}
export const TertiaryDisabledButton: Story = {
    args: {
        children: 'Tertiary disabled button',
        variant: 'tertiary',
        disabled: true,
    }
}

export const DestructiveButton: Story = {
    args: {
        children: 'Destructive button',
        variant: 'danger',
    }
}

export const DestructiveDisabledButton: Story = {
    args: {
        children: 'Destructive button',
        variant: 'danger',
        disabled: true,
    }
}


export const AllButtons: Story = {
    render: () => {
        return <div style={{ width: "100%", height: '100vh', background: "var(--background-default)", padding: '1rem' }}>
            <Button variant="primary">Primary Button</Button>{' '}
            <Button variant="secondary">Secondary Button</Button>{' '}
            <Button variant="tertiary">Tertiary Button</Button>{' '}
            <Button variant="danger">Error Button</Button>
        </div>
    },
    args: {}
}

export const IconButtons: Story = {
    render: () => {
        return <div style={{ width: "100%", height: '100vh', background: "var(--background-default)", padding: '1rem' }}>
            <Button variant="primary" icon={swordClashIcon}>Primary Button</Button>{' '}
            <Button variant="secondary" icon={swordClashIcon}>Secondary Button</Button>{' '}
            <Button variant="tertiary" icon={swordClashIcon}>Tertiary Button</Button>{' '}
            <Button variant="danger" icon={swordClashIcon}>Error Button</Button>
        </div>
    },
    args: {}
}
export const IconLeftButtons: Story = {
    render: () => {
        return <div style={{ width: "100%", height: '100vh', background: "var(--background-default)", padding: '1rem' }}>
            <Button variant="primary" icon={swordClashIcon} iconPosition="left">Primary Button</Button>{' '}
            <Button variant="secondary" icon={swordClashIcon} iconPosition="left">Secondary Button</Button>{' '}
            <Button variant="tertiary" icon={swordClashIcon} iconPosition="left">Tertiary Button</Button>{' '}
            <Button variant="danger" icon={swordClashIcon} iconPosition="left">Error Button</Button>
        </div>
    },
    args: {}
}

export const IconRightButtons: Story = {
    render: () => {
        return <div style={{ width: "100%", height: '100vh', background: "var(--background-default)", padding: '1rem' }}>
            <Button variant="primary" icon={swordClashIcon} iconPosition="right">Primary Button</Button>{' '}
            <Button variant="secondary" icon={swordClashIcon} iconPosition="right">Secondary Button</Button>{' '}
            <Button variant="tertiary" icon={swordClashIcon} iconPosition="right">Tertiary Button</Button>{' '}
            <Button variant="danger" icon={swordClashIcon} iconPosition="right">Error Button</Button>
        </div>
    },
    args: {}
}