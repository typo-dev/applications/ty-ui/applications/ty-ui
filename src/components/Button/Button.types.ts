import {MouseEventHandler, ReactNode} from "react";

export interface ButtonProps {
    /**
     * Button's label
     */
    children: string | ReactNode;
    /**
     * Function call on click on the button
     */
    onClick?: MouseEventHandler<HTMLButtonElement>,
    /**
     * variation of the button
     */
    variant?: 'danger' | 'primary' | 'secondary' | 'tertiary';
    /**
     * url de l'icône
     */
    icon?: string;
    /**
     * Position de l'icône
     */
    iconPosition?: 'left' | 'right';
}