import React, {ComponentProps, forwardRef} from "react";
import {InputGroupTypes} from "./Form.types";
import useCSSShortcut from "../hook/useCSSShortcut";
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";

const InputGroup = forwardRef<HTMLDivElement, InputGroupTypes & ComponentProps<"div"> & UseCSSShortcutTypes>(
    (
        {
            statut,
            style,
            children,
            className,
            ...otherProps
        },
        ref
    ) => {
        const inputGroupClasses = ['tyui-input-group'];

        if (statut) {
            inputGroupClasses.push('tyui-input-group--' + statut);
        }

        if (className) {
            inputGroupClasses.push(className);
        }

        const cssStyle = useCSSShortcut({...otherProps});

        const styleInputGroup = {...cssStyle, ...style};

        return <div {...otherProps} style={styleInputGroup} className={inputGroupClasses.join(" ")} ref={ref}>{children}</div>;
    }
);

export default InputGroup;