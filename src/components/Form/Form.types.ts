export interface InputGroupTypes {
    /**
     * statut du champs
     */
    statut?: 'success'|'warning'|'error';
}

export interface FormMessageTypes {
    /**
     * id obligatoire qui doit correspondre à la valeur du aria-describedby du champs
     */
    id: string;
}