import React, {ComponentProps, forwardRef} from "react";
import useCSSShortcut from "../hook/useCSSShortcut";
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";

const LabelHint = forwardRef<HTMLSpanElement, ComponentProps<"span"> & UseCSSShortcutTypes>(
    (
        {
            children,
            style,
            className,
            ...otherProps
        },
        ref
    ) => {
        const labelHintClasses = ['tyui-hint-text'];

        if (className) {
            labelHintClasses.push(className);
        }

        const cssStyle = useCSSShortcut({...otherProps});

        const styleLabelHint = {...cssStyle, ...style};

        return <span {...otherProps} style={styleLabelHint} className={labelHintClasses.join(" ")} ref={ref}>{children}</span>;
    }
);

export default LabelHint;