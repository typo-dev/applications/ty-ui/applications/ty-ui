import React, {ComponentProps, forwardRef} from "react";
import {FormMessageTypes} from "./Form.types";
import useCSSShortcut from "../hook/useCSSShortcut";
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";

const FormMessage = forwardRef<HTMLParagraphElement, FormMessageTypes & ComponentProps<"p"> & UseCSSShortcutTypes>(
    (
        {
            id,
            children,
            style,
            className,
            ...otherProps
        },
        ref
    ) => {
        const labelHintClasses = ['tyui-form-message'];

        if (className) {
            labelHintClasses.push(className);
        }

        const cssStyle = useCSSShortcut({...otherProps});

        const styleFormMessage = {...cssStyle, ...style};

        return <p {...otherProps} id={id} style={styleFormMessage} className={labelHintClasses.join(" ")} ref={ref}>{children}</p>;
    }
);

export default FormMessage;