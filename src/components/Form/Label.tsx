import React, {ComponentProps, forwardRef} from "react";
import useCSSShortcut from "../hook/useCSSShortcut";
import {UseCSSShortcutTypes} from "../hook/useCSSShortcut.types";

const Label = forwardRef<HTMLLabelElement, ComponentProps<"label"> & UseCSSShortcutTypes>(
    (
        {
            children,
            className,
            style,
            ...otherProps
        }, ref
    ) => {
        const labelClasses = ['tyui-label'];

        if (className) {
            labelClasses.push(className);
        }

        const cssStyle = useCSSShortcut({...otherProps});

        const styleLabel = {...cssStyle, ...style};

        return <label {...otherProps} style={styleLabel} className={labelClasses.join(" ")} ref={ref}>{children}</label>;
    }
);

export default Label;