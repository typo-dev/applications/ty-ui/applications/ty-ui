export interface UseCSSShortcutTypes {
    /**
     * Applique la même marge sur tous les côtés. Unité : rem
     */
    margin?: number|'auto';
    /**
     * Applique la même marge sur le côté gauche et droit. Unité : rem
     */
    marginX?: number|'auto';
    /**
     * Applique la même marge sur le haute et le bas. Unité : rem
     */
    marginY?: number|'auto';
    /**
     * Applique la marge haute. Unité : rem
     */
    marginTop?: number|'auto';
    /**
     * Applique la marge gauche. Unité : rem
     */
    marginLeft?: number|'auto';
    /**
     * Applique la marge droite. Unité : rem
     */
    marginRight?: number|'auto';
    /**
     * Applique la marge basse. Unité : rem
     */
    marginBottom?: number|'auto';
    /**
     * Applique la même marge sur tous les côtés. Unité : rem
     */
    m?: number|'auto';
    /**
     * Applique la même marge sur le côté gauche et droit. Unité : rem
     */
    mx?: number|'auto';
    /**
     * Applique la même marge sur le haute et le bas. Unité : rem
     */
    my?: number|'auto';
    /**
     * Applique la marge haute. Unité : rem
     */
    mt?: number|'auto';
    /**
     * Applique la marge gauche. Unité : rem
     */
    ml?: number|'auto';
    /**
     * Applique la marge droite. Unité : rem
     */
    mr?: number|'auto';
    /**
     * Applique la marge basse. Unité : rem
     */
    mb?: number|'auto';
    /**
     * Applique la même marge interne sur tous les côtés. Unité : rem
     */
    padding?: number|'auto';
    /**
     * Applique la même marge interne sur le côté gauche et droit. Unité : rem
     */
    paddingX?: number|'auto';
    /**
     * Applique la même marge interne sur le haute et le bas. Unité : rem
     */
    paddingY?: number|'auto';
    /**
     * Applique la marge interne haute. Unité : rem
     */
    paddingTop?: number|'auto';
    /**
     * Applique la marge interne gauche. Unité : rem
     */
    paddingLeft?: number|'auto';
    /**
     * Applique la marge interne droite. Unité : rem
     */
    paddingRight?: number|'auto';
    /**
     * Applique la marge interne basse. Unité : rem
     */
    paddingBottom?: number|'auto';
    /**
     * Applique la même marge interne sur tous les côtés. Unité : rem
     */
    p?: number|'auto';
    /**
     * Applique la même marge interne sur le côté gauche et droit. Unité : rem
     */
    px?: number|'auto';
    /**
     * Applique la même marge interne sur le haute et le bas. Unité : rem
     */
    py?: number|'auto';
    /**
     * Applique la marge interne haute. Unité : rem
     */
    pt?: number|'auto';
    /**
     * Applique la marge interne gauche. Unité : rem
     */
    pl?: number|'auto';
    /**
     * Applique la marge interne droite. Unité : rem
     */
    pr?: number|'auto';
    /**
     * Applique la marge interne basse. Unité : rem
     */
    pb?: number|'auto';
}