import {CSSProperties} from 'react';
import {UseCSSShortcutTypes} from "./useCSSShortcut.types";

const useCSSShortcut = (props: UseCSSShortcutTypes): CSSProperties => {
    const valueToRem = (value: number|'auto') => {
        if (value === 'auto') {
            return value;
        }
        return value + 'rem';
    }
    const styles: CSSProperties = {};
    if (props.margin !== undefined) styles.margin = valueToRem(props.margin);
    if (props.m !== undefined) styles.margin = valueToRem(props.m);
    if (props.marginX !== undefined) {
        styles.marginLeft = valueToRem(props.marginX);
        styles.marginRight = valueToRem(props.marginX);
    }
    if (props.mx !== undefined) {
        styles.marginLeft = valueToRem(props.mx);
        styles.marginRight = valueToRem(props.mx);
    }
    if (props.marginY !== undefined) {
        styles.marginTop = valueToRem(props.marginY);
        styles.marginBottom = valueToRem(props.marginY);
    }
    if (props.my !== undefined) {
        styles.marginTop = valueToRem(props.my);
        styles.marginBottom = valueToRem(props.my);
    }
    if (props.marginTop !== undefined) styles.marginTop = valueToRem(props.marginTop);
    if (props.mt !== undefined) styles.marginTop = valueToRem(props.mt);
    if (props.marginBottom !== undefined) styles.marginBottom = valueToRem(props.marginBottom);
    if (props.mb !== undefined) styles.marginBottom = valueToRem(props.mb);
    if (props.marginLeft !== undefined) styles.marginLeft = valueToRem(props.marginLeft);
    if (props.ml !== undefined) styles.marginLeft = valueToRem(props.ml);
    if (props.marginRight !== undefined) styles.marginRight = valueToRem(props.marginRight);
    if (props.mr !== undefined) styles.marginRight = valueToRem(props.mr);


    if (props.padding !== undefined) styles.padding = valueToRem(props.padding);
    if (props.p !== undefined) styles.padding = valueToRem(props.p);
    if (props.paddingX !== undefined) {
        styles.paddingLeft = valueToRem(props.paddingX);
        styles.paddingRight = valueToRem(props.paddingX);
    }
    if (props.px !== undefined) {
        styles.paddingLeft = valueToRem(props.px);
        styles.paddingRight = valueToRem(props.px);
    }
    if (props.paddingY !== undefined) {
        styles.paddingTop = valueToRem(props.paddingY);
        styles.paddingBottom = valueToRem(props.paddingY);
    }
    if (props.py !== undefined) {
        styles.paddingTop = valueToRem(props.py);
        styles.paddingBottom = valueToRem(props.py);
    }
    if (props.paddingTop !== undefined) styles.paddingTop = valueToRem(props.paddingTop);
    if (props.pt !== undefined) styles.paddingTop = valueToRem(props.pt);
    if (props.paddingBottom !== undefined) styles.paddingBottom = valueToRem(props.paddingBottom);
    if (props.pb !== undefined) styles.paddingBottom = valueToRem(props.pb);
    if (props.paddingLeft !== undefined) styles.paddingLeft = valueToRem(props.paddingLeft);
    if (props.pl !== undefined) styles.paddingLeft = valueToRem(props.pl);
    if (props.paddingRight !== undefined) styles.paddingRight = valueToRem(props.paddingRight);
    if (props.pr !== undefined) styles.paddingRight = valueToRem(props.pr);

    return styles;
}

export default useCSSShortcut;