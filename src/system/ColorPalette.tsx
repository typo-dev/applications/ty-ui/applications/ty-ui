import React, {CSSProperties, useMemo} from 'react'
import {Hct} from "@material/material-color-utilities";
import {Button, Input, InputGroup, Label} from "../components";

const ColorPalette = () => {
    const [hue, setHue] = React.useState<number>(156);

    const colorPalette = useMemo(() => {
        const colors = [];
        for (let i = 0; i <= 100; i = i + 2.5) {
            const hctObj = Hct.from(hue, 122, i);
            colors.push(hctObj.toInt().toString(16).slice(2));
        }
        return colors;
    }, [hue]);

    const greyColorPalette = useMemo(() => {
        const colors = [];
        for (let i = 0; i <= 100; i = i + 2.5) {
            const hctObj = Hct.from(hue, 5, i);
            colors.push(hctObj.toInt().toString(16).slice(2));
        }
        return colors;
    }, [hue]);

    return <>
        <InputGroup>
            <Label htmlFor="hue">Hue</Label>
            <Input
                id="hue"
                name="hue"
                onChange={(e) => {setHue(parseInt(e.target.value))}}
                value={hue}
                required
                type="number"
                min={0}
                max={360}
            />
        </InputGroup>
        <Button variant="primary" style={{
            "--primary-background": '#' + colorPalette[29],
            "--primary-hover": '#' + colorPalette[25],
            "--primary-active": '#' + colorPalette[21],
            color: '#' + colorPalette[1]
        } as CSSProperties}>Label button</Button>
        <div style={{ width: '100%', overflow: 'auto' }}>
            <table>
                <tbody>
                <tr>
                    {colorPalette.map((color) => {
                        return <td style={{
                            border: '1px solid black',
                            background: '#' + color,
                            height: '40px',
                            width: '15px'
                        }}></td>
                    })}
                </tr>
                <tr>
                    {greyColorPalette.map((color) => {
                        return <td style={{
                            border: '1px solid black',
                            background: '#' + color,
                            height: '40px',
                            width: '15px'
                        }}></td>
                    })}
                </tr>
                </tbody>
            </table>
        </div>
        <p>
            {colorPalette.map((color, index) => {
                return <>
                    <span>--primary-color-{(index * 25).toString().padStart(3, '0')}: {'#' + color + ';'}</span><br/></>
            })}
            {greyColorPalette.map((color, index) => {
                return <>
                    <span>--grey-{(index * 25).toString().padStart(3, '0')}: {'#' + color + ';'}</span><br/></>
            })}
        </p>
    </>
}

export default ColorPalette